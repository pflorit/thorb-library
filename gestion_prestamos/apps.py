from django.apps import AppConfig


class GestionPrestamosConfig(AppConfig):
    name = 'gestion_prestamos'
