from django.contrib import admin
from .models import Multa, Lector, Prestamo

admin.site.register(Multa)
admin.site.register(Lector)
admin.site.register(Prestamo)
