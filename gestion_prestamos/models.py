from django.db import models
from django.utils import timezone


class Multa(models.Model):
    inicio = models.DateTimeField(
        default=timezone.now)
    fin = models.DateTimeField(
        default=timezone.now)
    importe = models.PositiveSmallIntegerField


class Lector(models.Model):
    num_socio = models.AutoField(auto_created=True, primary_key=True)
    nombre_lector = models.CharField(max_length=200)
    telefono = models.PositiveSmallIntegerField

    # TO DO que estas mierda sean array
    libros_prestados = models.ForeignKey('libreria.Copia', on_delete=models.CASCADE, null=True)
    multas = models.ForeignKey('Multa', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nombre_lector


class Prestamo(models.Model):
    prestado_a = models.ForeignKey('Lector', on_delete=models.CASCADE, null=True)

    # TO DO que estas mierda sean array
    libros_prestados = models.ForeignKey('libreria.Copia', on_delete=models.CASCADE, null=True)

    inicio = models.DateTimeField(
        default=timezone.now)
    fin = models.DateTimeField(
        default=timezone.now)

    def __str__(self):
        return self.prestado_a.nombre_lector
