from django.db import models


class Libro(models.Model):
    autor = models.CharField(max_length=200)
    titulo = models.CharField(max_length=100)
    genero = models.CharField(max_length=100)
    editorial = models.CharField(max_length=100)
    anio = models.PositiveSmallIntegerField()
    num_copias = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.titulo


class Biblioteca(models.Model):
    nombre_biblioteca = models.CharField(max_length=200)

    # TO DO que esta mierda sea array
    libros = models.ForeignKey('Libro', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nombre_biblioteca


class Copia(models.Model):
    titulo_copia = models.ForeignKey('Libro', on_delete=models.CASCADE, null=False)
    STAT_CHOICES = [
        ('D', 'Disponible'),
        ('P', 'Prestado'),
        ('R', 'Retardo'),
        ('ND', 'No disponible'),
    ]
    estado_copia = models.CharField(max_length=2, choices=STAT_CHOICES, default=1)

    def __str__(self):
        return self.titulo_copia.titulo
