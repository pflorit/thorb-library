from django.contrib import admin
from .models import Libro, Biblioteca, Copia

admin.site.register(Libro)
admin.site.register(Biblioteca)
admin.site.register(Copia)
