from django.urls import path
from . import views

urlpatterns = [
    path('', views.home_page, name='home_page'),
    path('lista_libros', views.lista_libros, name='lista_libros'),
]
