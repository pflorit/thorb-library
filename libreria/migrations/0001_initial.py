# Generated by Django 2.2.12 on 2020-04-08 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('autor', models.CharField(max_length=200)),
                ('titulo', models.CharField(max_length=100)),
                ('genero', models.CharField(max_length=100)),
                ('editorial', models.CharField(max_length=100)),
                ('anio', models.PositiveSmallIntegerField()),
                ('num_copias', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Copia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(choices=[('D', 'Disponible'), ('P', 'Prestado'), ('R', 'Retardo'), ('ND', 'No disponible')], default=1, max_length=2)),
                ('titulo_copia', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='libreria.Libro')),
            ],
        ),
        migrations.CreateModel(
            name='Biblioteca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_biblioteca', models.CharField(max_length=200)),
                ('libros', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='libreria.Libro')),
            ],
        ),
    ]
