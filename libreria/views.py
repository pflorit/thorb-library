from django.shortcuts import render
from .models import Libro


def home_page(request):
    return render(request, 'libreria/home_page', {})


def lista_libros(request):
    libros = Libro.objects.filter().order_by('titulo')
    return render(request, 'libreria/lista_libros', {'libros': libros})
